package fr.freebox.motiss.minecraft.mod.summon_trader.block;

import fr.freebox.motiss.minecraft.mod.summon_trader.SummonTrader;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroupEntries;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class ModBlocks {
    public static final Block EMERALDED_BELL = registerBlock("emeralded_bell",
            new EmeraldedBell(FabricBlockSettings.copyOf(Blocks.BELL).nonOpaque()));

    public static void registerModBlock() {
        SummonTrader.LOGGER.info("registering block for " + SummonTrader.MOD_ID);
        ItemGroupEvents.modifyEntriesEvent(ItemGroups.FUNCTIONAL).register(ModBlocks::addItemsToFunctionalTabItemGroup);
    }

    private static void addItemsToFunctionalTabItemGroup(FabricItemGroupEntries entries) {
        entries.add(EMERALDED_BELL);
    }

    private static Block registerBlock(String name, Block block) {
        registerBlockItem(name, block);
        return Registry.register(Registries.BLOCK, new Identifier(SummonTrader.MOD_ID, name), block);
    }

    private static Item registerBlockItem(String name, Block block) {
        return Registry.register(
                Registries.ITEM,
                new Identifier(SummonTrader.MOD_ID, name),
                new BlockItem(block, new FabricItemSettings()));
    }
}
