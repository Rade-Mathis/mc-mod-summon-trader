package fr.freebox.motiss.minecraft.mod.summon_trader.block.entity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;

public class EmeraldedBellEntity extends BlockEntity {
    public EmeraldedBellEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntities.EMERALDED_BELL_ENTITY, pos, state);
    }

}
