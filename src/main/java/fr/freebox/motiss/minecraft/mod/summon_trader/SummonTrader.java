package fr.freebox.motiss.minecraft.mod.summon_trader;

import fr.freebox.motiss.minecraft.mod.summon_trader.block.ModBlocks;
import fr.freebox.motiss.minecraft.mod.summon_trader.block.entity.ModBlockEntities;
import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SummonTrader implements ModInitializer {
	public static final String MOD_ID = "summon-trader";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize() {
		LOGGER.info("Hello Fabric world!");
		ModBlocks.registerModBlock();
		ModBlockEntities.registerBlockEntities();
	}
}