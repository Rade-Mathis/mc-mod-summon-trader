package fr.freebox.motiss.minecraft.mod.summon_trader.block.entity;

import fr.freebox.motiss.minecraft.mod.summon_trader.SummonTrader;
import fr.freebox.motiss.minecraft.mod.summon_trader.block.ModBlocks;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class ModBlockEntities {
    public static final BlockEntityType<EmeraldedBellEntity> EMERALDED_BELL_ENTITY =
            Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier(SummonTrader.MOD_ID, "emeralded_bell_be"),
                    FabricBlockEntityTypeBuilder.create(
                            EmeraldedBellEntity::new,
                            ModBlocks.EMERALDED_BELL).build());

    public static void registerBlockEntities() {
        SummonTrader.LOGGER.info("registering block entities for " + SummonTrader.MOD_ID);
    }
}
